from __future__ import unicode_literals


import time

import youtube_dl
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from src.fileUtil import *

DOWNLOAD_VIDEO = True

DOWNLOADED_URL_LIST_FILE_NAME = "_url_list_"


DOWNLOAD_PATH = ".\\Downloads"
createFolderWithCheck(DOWNLOAD_PATH)


def downloadAllVideosFromChannel(channelName, opts, downloadedUrlList, savePath):
    downloadVideos(getAllVideosFromChannel(channelName, downloadedUrlList), opts, savePath)


def getAllVideosFromChannel(channelName, downloadedUrlList):
    driver = webdriver.Chrome(ChromeDriverManager().install())
    url = 'https://www.youtube.com/' + channelName + '/videos'
    driver.get(url)
    height = driver.execute_script("return document.documentElement.scrollHeight")
    previousHeight = -1
    while previousHeight < height:
        previousHeight = height
        driver.execute_script(f'window.scrollTo(0,{height + 10000})')
        time.sleep(1)
        height = driver.execute_script("return document.documentElement.scrollHeight")
    vidElements = driver.find_elements_by_id('thumbnail')
    vid_urls = []
    for v in vidElements:
        url = v.get_attribute('href')
        if url != None and not url in downloadedUrlList:
            vid_urls.append(url)
    return vid_urls


def getVideoInfo(link, opts):
    from youtube_dl import YoutubeDL
    with YoutubeDL(opts) as ydl:
        info_dict = ydl.extract_info(link, download=False)
        video_url = info_dict.get("url", None)
        video_id = info_dict.get("id", None)
        video_title = info_dict.get('title', None)
        print(video_url)
        print(video_id)
        print(video_title)


def downloadVideo(link, opts):
    if link == None:
        return
    try:
        if DOWNLOAD_VIDEO:
            with youtube_dl.YoutubeDL(opts) as ydl:
                ydl.download([link])
        return True
    except:
        print("Exception!")
        return False


def downloadVideos(videos, opts, savePath):
    if len(videos) == 0:
        print("No new videos found!")
    counter = 1
    for video in videos:
        print(str(counter) + "/" + str(len(videos)))
        if downloadVideo(video, opts):
            if type(video) == str:
                append_new_line(savePath + "\\" + DOWNLOADED_URL_LIST_FILE_NAME + ".txt", video)
        counter += 1
