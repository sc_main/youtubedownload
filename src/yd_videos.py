from src.yd_base import *
from src.youtubeDownload import *

SAVE_PATH = ".\\Downloads\\_VariousVideos_"
createFolderWithCheck(SAVE_PATH)

ydl_opts["outtmpl"] = SAVE_PATH + "/%(title)s.%(ext)s'"

downloadList = ["https://www.youtube.com/watch?v=QtFYT4gXGaA"]
downloadVideos(downloadList, ydl_opts, SAVE_PATH)
