from src.youtubeDownload import *
from src.yd_base import *

channelList = []
channelList.append({"channel": "AKINALTAN",
                    "folder": "akin_altan",
                    "description": "Akın Altan"})
channelList.append({"channel": "c/SesliKitapDünyası",
                    "folder": "sesli_kitap_dunyasi",
                    "description": "Sesli Kitap Dünyası"})
channelList.append({"channel": "channel/UCEGZkwRlpuQ1fndGaI1zhpg",
                    "folder": "kitap_rafi",
                    "description": "Kitap Rafi"})
channelList.append({"channel": "channel/UCkQ8KcgpyyZ6xOuOsRbQERg",
                    "folder": "emel_kalender",
                    "description": "Emel Kalender"})
channelList.append({"channel": "channel/UCJrxibTYAfSlw2aLDWVeKrg",
                    "folder": "bir_dinle",
                    "description": "Bir Dinle"})
channelList.append({"channel": "c/BizimKütüphane",
                    "folder": "bizim_kutuphane",
                    "description": "Bizim Kutuphane"})

for channel in channelList:
    print("===== " + channel["description"] + " =====")
    SAVE_PATH = ".\\Downloads\\" + channel["folder"]
    createFolderWithCheck(SAVE_PATH)

    ydl_opts["outtmpl"] = SAVE_PATH + "/%(title)s.%(ext)s'"
    try:
        downloadedUrlList = readTextFile(SAVE_PATH, DOWNLOADED_URL_LIST_FILE_NAME)
        for i in range(len(downloadedUrlList)):
            downloadedUrlList[i] = downloadedUrlList[i].replace("\n", "")
    except:
        writeTextToFile("", SAVE_PATH + "\\" + DOWNLOADED_URL_LIST_FILE_NAME + ".txt")
        downloadedUrlList = []
    for file in os.listdir(SAVE_PATH):
        parts = file.split("-")
        lastPart = parts[-1]
        lastPart = lastPart.split(".")[0]
        link = "https://www.youtube.com/watch?v=" + lastPart
        downloadedUrlList.append(link)
    print(str(len(downloadedUrlList)) + " files already downloaded.")
    downloadAllVideosFromChannel(channel["channel"], ydl_opts, downloadedUrlList, SAVE_PATH)
